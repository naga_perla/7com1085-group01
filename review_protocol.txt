# Search Protocol - Group 01

Search string

~~~~~ {.searchstring }
((“vehicular ad hoc networks” OR "vanet") AND “fog computing” AND “latency”)
~~~~~

Number of papers: 70

Inclusion criteria:

1. Must directly answer the research question
2. Must have been published in the last ten years from now
3. Must be a peer-reviewed conference or journal
4. Must be an empirical study


Exclusion criteria:

1. If publication is duplicate, include only the latest, or more recent one
